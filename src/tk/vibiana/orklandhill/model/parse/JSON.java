package tk.vibiana.orklandhill.model.parse;
import android.annotation.SuppressLint;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tk.vibiana.oaklandhill.model.ChannelListing;
import tk.vibiana.oaklandhill.model.Story;

public class JSON {
	
	
	
	private static InputStream inputstream = null;
	private static JSONObject JSONSource = null;
	private static String JSONFeed = "";
	private static final  String APITOKEN = "&apiKey=";
	public static InputStreamReader getInputStreamReader(String source) throws ClientProtocolException, IOException{
		DefaultHttpClient client = new DefaultHttpClient(new BasicHttpParams());
		HttpPost method = new HttpPost(source);
		method.setHeader("Content-type", "application/json");
		HttpResponse clientResponse = client.execute(method);
		HttpEntity httpEntity = clientResponse.getEntity();
	    inputstream = httpEntity.getContent();
		
		return new InputStreamReader(inputstream, "UTF-8");
		
	}
	public static JSONObject getDataSource(InputStreamReader inputStreamReader) throws IOException, JSONException{
		BufferedReader reader = new BufferedReader(inputStreamReader,8);
		StringBuilder stringbuilder = new StringBuilder();
		String line = null;
		while((line= reader.readLine()) != null){
			stringbuilder.append(line);
		}
		inputstream.close();
		JSONFeed = stringbuilder.toString();
		JSONObject JSONSource = new JSONObject(JSONFeed);
		return JSONSource;	
	}
	public static ChannelListing parse(String dataSourceURL) throws ClientProtocolException, IOException, JSONException, ParseException{
		ChannelListing channel = parseChannel(dataSourceURL);

		return channel;
	}
	private static ChannelListing parseChannel(String dataSourceURL) throws ClientProtocolException, IOException, JSONException, ParseException{
		JSONObject jsonObject = getDataSource(getInputStreamReader(dataSourceURL));
		
		//Setting Channel Title
		JSONObject listObject = jsonObject.getJSONObject("list");
		JSONObject getterObject = listObject.getJSONObject("title");
		String text = getterObject.getString("$text");
		ChannelListing channel =  new ChannelListing(jsonObject.getString("version"));
		channel.setTitle(text);
		
		//Setting Channel Description
		getterObject = listObject.getJSONObject("teaser");
		text = getterObject.getString("$text");
		channel.setDescription(text);
		
		//Setting APITrace
		JSONArray getterArray = listObject.getJSONArray("link");
		getterObject = getterArray.getJSONObject(0);
		text = getterObject.getString("$text");
		text = text.substring(text.lastIndexOf(APITOKEN)+APITOKEN.length());
		channel.setAPITrace(text);
		
		//Setting Channel Link
		getterArray = listObject.getJSONArray("link");
		getterObject = getterArray.getJSONObject(1);
		text = getterObject.getString("$text");
		channel.setLink(text);
		
		JSONArray storyArray = listObject.getJSONArray("story");
		
		for(int i = 0; i < storyArray.length(); i++){
			JSONObject storyFeed = storyArray.getJSONObject(i);
			Story story = parseStory(storyFeed);
			channel.add(story);
		}
		return channel;
	}
	private static Story parseStory(JSONObject storyFeed) throws JSONException, MalformedURLException, ParseException{
		Story story;
		story = new Story(storyFeed.getString("id"));
		JSONArray linkArray = storyFeed.getJSONArray("link");
		story.setShortLink(new URL(linkArray.getJSONObject(2).getString("$text")));
		story.setDescription(storyFeed.getJSONObject("teaser").getString("$text"));

		story.setTitle(storyFeed.getJSONObject("title").getString("$text"));
		story.setProgram(storyFeed.getJSONArray("show").getJSONObject(0).getJSONObject("program").getString("$text"));
		String storyDate = storyFeed.getJSONObject("storyDate").getString("$text");
		story.setStoryDate(parseDate(storyDate));
		ArrayList<String> paragraphs = new ArrayList<String>();
		for(int i = 0;i < storyFeed.getJSONObject("textWithHtml").getJSONArray("paragraph").length(); i++){
			paragraphs.add(storyFeed.getJSONObject("textWithHtml").getJSONArray("paragraph").getJSONObject(i).getString("$text"));
		}
		
		story.setParagraphs(paragraphs);
		String m3ufile = storyFeed.getJSONArray("audio").getJSONObject(0).getJSONObject("format").getJSONArray("mp3").getJSONObject(0).getString("$text");
		story.setM3ufile(new URL(m3ufile));
		String mp4file = storyFeed.getJSONArray("audio").getJSONObject(0).getJSONObject("format").getJSONObject("mp4").getString("$text");
		story.setMp4file(new URL(mp4file));
		String image = storyFeed.getJSONArray("image").getJSONObject(0).getString("src");
		story.setImage(new URL(image));
		String byline = storyFeed.getJSONArray("byline").getJSONObject(0).getJSONObject("name").getString("$text");
		story.setByline(byline);


		return story;
		
	}
	@SuppressLint("SimpleDateFormat")
	private static Date parseDate(String date) throws ParseException{
		//Parsing "Sat, 10 Aug 2013 12:00:00 -0400"
		SimpleDateFormat dateFormat = new SimpleDateFormat("E, d MMM yyyy k:m:s ZZZZZ");
		Date returnable = dateFormat.parse(date);
		return returnable;
		
	}
	public static JSONObject getJSONSource() {
		return JSONSource;
	}
	public static void setJSONSource(JSONObject jSONSource) {
		JSONSource = jSONSource;
	}

}
