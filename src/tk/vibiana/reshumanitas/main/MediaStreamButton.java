package tk.vibiana.reshumanitas.main;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

 
public class MediaStreamButton extends Fragment {
	  private String mediaURL = null;
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {
		  View MediaStreamButton = inflater.inflate(R.layout.media_stream_button, container, false);
	    return MediaStreamButton;
	  }
	public String getMediaURL() {
		return mediaURL;
	}
	public void setMediaURL(String mediaURL) {
		this.mediaURL = mediaURL;
	}
	  



}
