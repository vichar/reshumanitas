package tk.vibiana.reshumanitas.main;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import tk.vibiana.oaklandhill.model.ChannelListing;
import tk.vibiana.oaklandhill.model.Story;
import tk.vibiana.orklandhill.model.parse.JSON;
import tk.vibiana.reshumanitas.main.R;

public class StreamActivity extends Activity {
	
    public final static String EXTRA_STORY = "tk.vibiana.reshumanitas.main.STORY";
    ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		dialog = ProgressDialog.show(this, "Loading",
		            "Loading, please wait..");
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
		setContentView(R.layout.activity_stream);
		new PrepareChannel().execute();
	}

	class StreamItem {		
		final URL bitmapURL;
		final String streamTitle;
		final String streamDate;
		final String streamText;
		StreamItem(Context c, URL value, String title, String date, String text) {
			bitmapURL = value;
			streamTitle = title;
			streamDate = date;
			streamText = text;
		}
	}
	class StreamAdapter extends ArrayAdapter<StreamItem> {
		private final LayoutInflater mInflater;

		public StreamAdapter(Context context) {
			super(context, 0);
			mInflater = LayoutInflater.from(getContext());
		}	

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewGroup view = null;
			
			if (convertView == null) {
				view = (ViewGroup) mInflater.inflate(R.layout.stream_item, parent, false);
			} else {
				view = (ViewGroup) convertView;
			}

			StreamItem item = getItem(position);
				try {
					FetchBitmap fetchbitmap = new FetchBitmap(item.bitmapURL);
					Thread thread = new Thread(fetchbitmap);
					thread.run();
					thread.join();
					Bitmap remoteImage = fetchbitmap.getBitmap();
					((ImageView) view.findViewById(R.id.thumbnail)).setImageBitmap(remoteImage);

				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				((TextView) view.findViewById(R.id.listItemTitle)).setText(item.streamTitle);
				((TextView) view.findViewById(R.id.listItemDate)).setText(item.streamDate);
				((TextView) view.findViewById(R.id.listItemText)).setText(item.streamText);  


			LayoutParams lp = view.getLayoutParams();
			lp.width = getContext().getResources().getDisplayMetrics().widthPixels;

			return view;
		}
	}
	private class FetchBitmap implements Runnable {
	    private Bitmap image = null;
	    private URL imagesource = null;
	    public FetchBitmap(URL bitmapURL) throws MalformedURLException{
	    	imagesource = bitmapURL;
	    	
	    }
	    public void run() {
			Log.d("Image URL", imagesource.toString());

	        try {
				HttpURLConnection connection = (HttpURLConnection) imagesource.openConnection();
				connection.setDoInput(true);
			    connection.connect();
			    InputStream input = connection.getInputStream();
			    image = BitmapFactory.decodeStream(input);
				Log.d("Image URL", "Image Params"+image.getHeight());

			} catch (Exception e) {
				e.printStackTrace();
				image = BitmapFactory.decodeResource(getResources(), R.drawable.default_image);
			
			}

	    }
	    public Bitmap getBitmap() { 
	    	return image; 
	    }
	
	}

		private class PrepareChannel extends AsyncTask<String, String, ChannelListing>{
		    private ChannelListing channel = null;
			@Override
			protected ChannelListing doInBackground(String... arg0) {
			    
				channel = new ChannelListing("2.0");
				return channel;
			}
			protected void onPostExecute(final ChannelListing channel){
				final StreamAdapter adapter = new StreamAdapter(StreamActivity.this);
				ListView main_list = ((ListView) findViewById(R.id.main_list));
				main_list.setAdapter(adapter);

				
				for (int i = 0; i < channel.size(); i++) {
					Story story = channel.get(i);
					DateFormat dateParser =  DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US);
					DateFormat timeParser = DateFormat.getTimeInstance(DateFormat.SHORT);
					StringBuilder datetime = new StringBuilder();
					datetime.append(dateParser.format(story.getStoryDate())+", ");
					datetime.append(timeParser.format(story.getStoryDate())+".");

						adapter.add(
						  new StreamItem(
								  			StreamActivity.this, 
								  			story.getImage(),
								  			story.getTitle(),
											datetime.toString(),
								  			story.getDescription())
					);
				}
				main_list.setOnItemClickListener(
						new AdapterView.OnItemClickListener() {

							@Override
							public void onItemClick(
									AdapterView<?> parent,
									final View view, 
									int position, 
									long id) 
							{
								//Log.d("onItemClick", "onItemClick Success");
								Intent intent = new Intent(StreamActivity.this,StoryActivity.class);
								
								Story s = channel.search(((TextView) view.findViewById(R.id.listItemTitle)).getText().toString());
								intent.putExtra(EXTRA_STORY,s);
							    startActivity(intent);

							}
						}
				);
				dialog.dismiss();
				
			
			}
		}
}
	
			

