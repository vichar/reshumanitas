package tk.vibiana.reshumanitas.main;

import java.text.DateFormat;
import java.util.Locale;
import tk.vibiana.reshumanitas.main.MediaStreamControl;



import tk.vibiana.oaklandhill.model.Story;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import tk.vibiana.reshumanitas.main.MediaStreamButton;


public class StoryActivity extends Activity {
	private Story story;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_story);
		 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	            // Show the Up button in the action bar.
	            getActionBar().setDisplayHomeAsUpEnabled(true);
	      }

		 
	    //Get the message from the intent
	   Intent intent = getIntent();
	   story  = (Story) intent.getSerializableExtra(StreamActivity.EXTRA_STORY);
	   WebView browser = (WebView)findViewById(R.id.storyText);
	   WebSettings mWebSettings = browser.getSettings();
	   mWebSettings.setBuiltInZoomControls(true);
	   browser.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
	   browser.setScrollbarFadingEnabled(true);
	   StringBuilder content = new StringBuilder();
	   content.append("<h1>"+story.getTitle()+"</h1>"); 
	   DateFormat df = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.US);
	   content.append("<h5>"+df.format(story.getStoryDate())+"</h5>");
	   content.append("<h5>"+story.getByline()+"</h5>");
	   content.append("<h5>"+"<img src=\""+story.getImage().toString()+"\"></h5>");
	   for(int i = 0; i< story.getParagraphs().size(); i++){
		   content.append("<p>"+story.getParagraphs().get(i)+"</p>");
	   } 
	   browser.loadData(content.toString(),"text/html", "UTF-32");
	   FragmentManager fragmentManager = getFragmentManager();
	   FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
	   MediaStreamButton button = new MediaStreamButton();
	   fragmentTransaction.add(R.id.fragment_placeholder, button);
	   fragmentTransaction.commit();
	  
}
	



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.story, menu);
		return true;
	}	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		   case android.R.id.home:
			      finish();
			      return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void retrieveIntent(View v) {
		  Log.d("Retrieve Intent Data", "OK");
		  MediaStreamControl mediaStreamControl = new MediaStreamControl();
		  mediaStreamControl.setMediaURL(story.getMp4file().toString());

		  FragmentManager fragmentManager = getFragmentManager();
		  FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		  fragmentTransaction.replace(R.id.fragment_placeholder,mediaStreamControl, "MediaStreamController");
		  fragmentTransaction.commit();

		 
	 }

}
