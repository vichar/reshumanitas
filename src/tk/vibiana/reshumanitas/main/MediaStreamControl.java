package tk.vibiana.reshumanitas.main;

import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

 
public class MediaStreamControl extends Fragment {
	  private String mediaURL = null;

	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	      Bundle savedInstanceState) {
		  View mediaStreamControlPanel = inflater.inflate(R.layout.media_stream_control, container, false);
		  Log.d(getMediaURL(), "Imported Data");
		  MediaPlayer mediaPlayer = new MediaPlayer();
		  mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		  try {
			mediaPlayer.setDataSource(getMediaURL());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  try {
			mediaPlayer.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // might take long! (for buffering, etc)
		  mediaPlayer.start();

	      return mediaStreamControlPanel;
	  }

	public String getMediaURL() {
		return mediaURL;
	}

	public void setMediaURL(String mediaURL) {
		this.mediaURL = mediaURL;
	}



}
