package tk.vibiana.oaklandhill.model;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

public class Story implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7937499058920132412L;
	private String id;
	private URL article;
	private URL APITrace;
	private URL shortLink;
	private String description;
	private String program;
	private Date storyDate;
	private String text;
	private ArrayList<String> paragraphs;
	private URL mp4file;
	private URL m3ufile;
	private URL image;
	private String title;
	private String byline;
	public Story(String value) {
		id = value;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public URL getArticle() {
		return article;
	}


	public void setArticle(URL article) {
		this.article = article;
	}


	public URL getAPITrace() {
		return APITrace;
	}


	public void setAPITrace(URL aPITrace) {
		APITrace = aPITrace;
	}


	public URL getShortLink() {
		return shortLink;
	}


	public void setShortLink(URL shortLink) {
		this.shortLink = shortLink;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getProgram() {
		return program;
	}


	public void setProgram(String program) {
		this.program = program;
	}


	public Date getStoryDate() {
		return storyDate;
	}


	public void setStoryDate(Date storyDate) {
		this.storyDate = storyDate;
	}


	public URL getMp4file() {
		return mp4file;
	}


	public void setMp4file(URL mp4file) {
		this.mp4file = mp4file;
	}


	public URL getM3ufile() {
		return m3ufile;
	}


	public void setM3ufile(URL m3ufile) {
		this.m3ufile = m3ufile;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public URL getImage() {
		return image;
	}


	public void setImage(URL image) {
		this.image = image;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}



	public ArrayList<String> getParagraphs() {
		return paragraphs;
	}


	public void setParagraphs(ArrayList<String> paragraphs) {
		this.paragraphs = paragraphs;
	}


	public String getByline() {
		return byline;
	}


	public void setByline(String byline) {
		this.byline = byline;
	}


}
