package tk.vibiana.oaklandhill.model;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

public class ChannelListing extends ArrayList<Story> {

	private static final long serialVersionUID = 587391619177158087L;
	private String title;
	private String description;
	private String APITrace;
	private String APIVersion;
	private URL Link;
	
	public ChannelListing() {
	}

	public ChannelListing(String value) {
		APIVersion = value;
	}
	public ChannelListing(int capacity) {
		super(capacity);
		// TODO Auto-generated constructor stub
	}

	public ChannelListing(Collection<Story> collection) {
		super(collection);
		// TODO Auto-generated constructor stub
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getAPITrace() {
		return APITrace;
	}


	public void setAPITrace(String aPITrace) {
		APITrace = aPITrace;
	}


	public String getAPIVersion() {
		return APIVersion;
	}


	public void setAPIVersion(String aPIVersion) {
		APIVersion = aPIVersion;
	}


	public URL getLink() {
		return Link;
	}


	public void setLink(String value) throws MalformedURLException {
		Link = new URL(value);
	}
	public Story search(String title){
		Story story = null;
		for(int i=0; i < this.size(); i++){
			Story temp = this.get(i);
			if(temp.getTitle().equals(title)){
				story = temp;
			}
		}
		return story;
	}



}
